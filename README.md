# IEX Cloud

A CHICKEN Scheme client library for [IEX Cloud][iexcloud.io].

[iexcloud.io]: https://iexcloud.io/

## Installation

    $ git clone http://git.foldling.org/chicken-iex.git
    $ cd chicken-iex
    $ chicken-install

## Usage

There's no documentation, sorry. If you're interested in using this
library, bug the author and he'll write some.

## License

This software is licensed under the [3-clause BSD license][license].

[license]: https://opensource.org/licenses/BSD-3-Clause
