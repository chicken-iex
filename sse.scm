;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Server-sent Events client
;;;
;;; Copyright (c) 2019, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare (module (server-sent-events client)))

(import (chicken condition)
        (chicken io)
        (chicken irregex)
        (chicken module)
        (chicken string)
        (chicken tcp)
        (openssl)
        (http-client)
        (intarweb)
        (srfi 18)
        (uri-common))

(export read-server-sent-event
        call-with-server-sent-events
        close-server-sent-events-connection-condition)

(define (read-server-sent-event #!optional (port (current-input-port)))
  (let loop ((retry #f)
             (id #f)
             (event "message")
             (data '()))
    (let ((line (read-line port)))
      (cond
        ;; disconnect
        ((eof-object? line)
         (values #f #f id retry))
        ;; dispatch
        ((irregex-match '(* space) line)
         (values (string-intersperse (reverse data) "\n") (string->symbol event) id retry))
        ;; message
        ((irregex-match '(: ($ (+ (~ #\:)))
                            (or (* space)
                                (: #\: (? space) ($ (+ any)))))
                        line)
         => (lambda (m)
              (let ((field (irregex-match-substring m 1))
                    (value (irregex-match-substring m 2)))
                (case (string->symbol field)
                  ((id)    (loop retry value event data))
                  ((event) (loop retry id value data))
                  ((retry) (loop (string->number value) id event data))
                  ((data)  (loop retry id event (if (not value)
                                                    (list)
                                                    (cons value data))))
                  (else    (loop retry id event data))))))
        ;; ignore
        (else
         (loop retry id event data))))))

(define (close-server-sent-events-connection-condition . args)
  (apply condition '(server-sent-events) '(close-connection) args))

(define (close-server-sent-events-connection-condition? x)
  (and (condition? x)
       ((condition-predicate 'server-sent-events) x)
       ((condition-predicate 'close-connection) x)))

(define (http-server-error-condition? x)
  (and (condition? x)
       ((condition-predicate 'http) x)
       (or ((condition-predicate 'server-error) x)
           ((condition-predicate 'unexpected-server-response) x))))

(define (call-with-server-sent-events uri-or-request handler)
  (##sys#check-closure handler 'call-with-server-sent-events)
  (let ((id 0) (retry 2000))
    (let loop ()
      (and (handle-exceptions
               x (cond ((close-server-sent-events-connection-condition? x) #f)
                       ((http-server-error-condition? x) #t)
                       (else (signal x)))
             (parameterize ((tcp-read-timeout #f))
               (call-with-input-request
                (update-request
                 (cond ((request? uri-or-request) uri-or-request)
                       ((uri? uri-or-request) (make-request uri: uri-or-request))
                       ((string? uri-or-request) (make-request uri: (uri-reference uri-or-request)))
                       (else (abort (condition '(type) `(exn message "bad argument type - not a uri"
                                                             location call-with-server-sent-events
                                                             arguments (,uri-or-request))))))
                 (replace-header-contents
                  'accept '(#(text/event-stream ()))
                  (replace-header-contents
                   'last-event-id `(#(,id ()))
                   (if (request? uri-or-request)
                       (request-headers uri-or-request)
                       (headers '())))))
                #f
                (lambda (port)
                  (let loop ()
                    (receive (data type id* retry*) (read-server-sent-event port)
                      (when id* (set! id id*))
                      (when retry* (set! retry retry*))
                      (when data (and (handler type data) (loop)))))))))
           (when (positive? retry) (thread-sleep! (/ retry 1000)))
           (loop)))))
