;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; IEX Cloud API client library
;;;
;;; Copyright (c) 2019, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare (module (iex)))

(import (chicken module)
        (chicken string)
        (openssl)
        (http-client)
        (medea)
        (server-sent-events client)
        (uri-common))

(export make-client client fetch stream)

(define +uri+ (uri-reference "https://cloud.iexapis.com"))
(define +sse-uri+ (uri-reference "https://cloud-sse.iexapis.com"))
(define +sandbox-uri+ (uri-reference "https://sandbox.iexapis.com"))
(define +sandbox-sse-uri+ (uri-reference "https://sandbox-sse.iexapis.com"))

(define-record client api-version secret-token uri sse-uri)

(define-inline (request-path client path)
  `(/ ,(client-api-version client)
      .
      ,(if (string? path)
           (string-split path "/")
           (map ->string path))))

(define-inline (request-query client query)
  `((token . ,(client-secret-token client)) . ,query))

(define make-client
  (let ((make-client make-client))
    (lambda (api-version secret-token #!optional sandbox)
      (make-client api-version secret-token
                   (if sandbox +sandbox-uri+ +uri+)
                   (if sandbox +sandbox-sse-uri+ +uri+)))))

(define (fetch client path query)
  (parameterize ((form-urlencoded-separator "&"))
    (nth-value
     0
     (call-with-input-request
      (update-uri (client-uri client)
                  path: (request-path client path)
                  query: (request-query client query))
      #f
      (lambda (port)
        (read-json port))))))

(define (stream client path query handler)
  (parameterize ((form-urlencoded-separator "&"))
    (call-with-server-sent-events
     (update-uri (client-sse-uri client)
                 path: (request-path client path)
                 query: (request-query client query))
     (lambda (type data)
       (handler type (read-json data))))))
